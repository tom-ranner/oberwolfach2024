BUILDDIR=public

THEME=metropolis
BIBLIOGRAPHY=./bib/library.bibtex
TEMPLATE=./pandoc/revealjs-template.html

HTML_TARGETS=index.html
ALL_HTML_TARGETS=index.html index.full.html index.embed.html

THEME_CSS=./static/css/$(THEME).css

PANDOC=pandoc \
	-t revealjs \
	-V revealjsurl=./static/js/revealjs \
	--template=$(TEMPLATE) \
	--slide-level 2 \
	-V transition=none \
	-V backgroundTransition=none \
	-V autoPlayMedia=true \
	-V css=$(THEME_CSS) \
	-V center=true \
	-V history \
	--citeproc \
	--bibliography=$(BIBLIOGRAPHY) \
	--csl=./bib/ima.csl \
	--filter ./pandoc/video-filter.py \
	-V reference-section-title=References \
	-V link-citations=true \
	--mathjax \
	-V mathjaxurl=./static/js/mathjax/es5/tex-chtml-full.js \

TARGETS=${HTML_TARGETS}

all: ${TARGETS}

%.html: %.md pandoc/video-filter.py $(BIBLIOGRAPHY) $(TEMPLATE)
	$(PANDOC) \
	-V controls=false \
	-V controlsTutorial=false \
	-V slideNumber="'c'" \
	-s $< -o $@

%.embed.html: %.md $(BIBLIOGRAPHY) $(TEMPLATE)
	$(PANDOC) \
	-t revealjs \
	-V controls=true \
	-V controlsTutorial=true \
	-V slideNumber=false \
	-V embedded="true" \
	-V print-pdf=false \
	-s $< -o $@

%.full.html: %.md $(BIBLIOGRAPHY) $(TEMPLATE) $(THEME_CSS)
	$(PANDOC) \
	--self-contained \
	-V controls=false \
	-V slideNumber="'c'" \
	-s $< -o $@

./static/css/%.css: ./pandoc/%.scss
	mkdir -p ./static/css
	pysassc $< $@

build: ${ALL_HTML_TARGETS} $(THEME_CSS)
	mkdir -p ${BUILDDIR}
	cp -v ${ALL_HTML_TARGETS} ${BUILDDIR}/.
	cp -rv static ${BUILDDIR}/static

quickbuild: ${HTML_TARGETS} $(THEME_CSS)
	mkdir -p ${BUILDDIR}
	cp -v ${HTML_TARGETS} ${BUILDDIR}/.
	cp -rv static ${BUILDDIR}

.PHONY: build

clean:
	rm -f ${TARGETS}
	rm -rf ${BUILDDIR}
	rm -f ${THEME_CSS}

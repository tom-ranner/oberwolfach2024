#!/usr/bin/env python
import os
import sys

import ffmpeg
from pandocfilters import Image, RawInline, toJSONFilter


def htmlblock(code):
    return RawInline("html", code)


def prefixed(fn):
    d = os.path.dirname(fn)
    b = os.path.basename(fn)
    return os.path.join(d, "." + b)


def convert_to_mp4(src: str) -> str | None:
    src_name, _ = os.path.splitext(src)
    out_fn = prefixed(src_name) + ".mp4"
    sys.stderr.write(f"{src} -> {out_fn}\n")

    if os.path.isfile(out_fn):
        sys.stderr.write(f"SKIPPING {src} -> {out_fn}. File already exists!\n")
        return out_fn

    (
        ffmpeg.input(src)
        .output(
            out_fn,
            vcodec="libx264",
            profile="baseline",
            level="3.0",
            movflags="faststart",
            crf=22,
        )
        .run()
    )

    if os.path.isfile(out_fn):
        return out_fn
    return None


def attrs_to_string(attrs):
    ret = ""

    for a in attrs:
        if isinstance(a, str):
            o = a.strip()
            if o != "":
                ret = ret + f'class="{o}"' + " "
        elif isinstance(a, list):
            if len(a) == 2:
                ret = ret + f'{a[0]}="{a[1]}"' + " "
            else:
                ret = ret + attrs_to_string(a) + " "

    return ret.strip()


def video_filter(key, value, format, _):
    if key == "Image":
        if len(value) == 2:
            alt, [src, title] = value
            attrs = None
        else:
            attrs, alt, [src, title] = value

        if format in ("html", "html5", "revealjs"):
            _, src_ext = os.path.splitext(src)
            if src_ext in [".mp4", ".avi", ".webm", ".ogv"]:  # we have a video
                out_list = [src]

                out = convert_to_mp4(src)
                if out is not None:
                    out_list.append(out)

                attr_str = attrs_to_string(attrs)
                return htmlblock(
                    "<video {}>\n".format(attr_str)
                    + "\n".join([f'<source data-src="{out}" />' for out in out_list])
                    + "\n"
                    + '<a href="{}">Video</a>\n'.format(src)
                    + "</video>"
                )
        else:
            if attrs:
                return Image(attrs, alt, [src, title])
            else:
                return Image(alt, [src, title])


if __name__ == "__main__":
    toJSONFilter(video_filter)

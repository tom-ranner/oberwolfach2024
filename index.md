---
author:
- name: Tom Ranner
  address: School of Computing, University of Leeds, UK
  email: T.Ranner@leeds.ac.uk
  with: Netta Cohen, Lukas Deutz, Thomas P. Ilett, and Yongxing Wang
title: Understanding microswimmer locomotion with Cosserat rods
footer: "T. Ranner: Cosserat rod microswimmer"
lang: en-GB
logos:
- source: ./static/img/Leeds_Logo.svg
  alt: University of Leeds
- source: ./static/img/EPSRC.png
  alt: EPSRC
# mathjax
include-before: |
  <div style="display:none">
  $$
    \renewcommand{\vec}[1]{\boldsymbol{#1}}
    \newcommand{\R}{\mathbb{R}}
    \newcommand{\highlight}[1]{{\color{##a92c6d} #1}}
    \newcommand{\highlightsecond}[1]{{\color{##00356b} #1}}
    \newcommand{\diag}{\operatorname{diag}}
    \newcommand{\COT}{\mathrm{COT}}
    \newcommand{\BMR}{\mathrm{BMR}}
  $$
  </div>
---

# One special nematode: *C. elegans* { data-background="./static/img/worm.jpg" class="lighttitle"}

## A little bit of biology

::: container
::: col
::: { style="font-size:60%" }
![Image of *C. elegans* through a normal microscope](./static/img/worm.jpg){ width=70% }

![3D experimental set up](./static/img/getting-data.jpg){ width=70% }
:::
:::
::: col
For over 50 years, used as a genetic model for understanding developmental biology and neurobiology.

Approx. 1000 cells, 302 neurons but freely living!
Invariant wild-type cell lineage and neuroanatomy

Natural habitat: rotting vegetation or soil
1mm long, eyelash width, transparent
:::
:::

## Midline reconstruction [@Ilett2023] i

![](./static/img/clip.png){ .r-stretch }

## Midline reconstruction [@Ilett2023] ii

![](./static/video/clip.mp4){ width=100% }

## The questions

1. What can we capture about this locomotion in a model?

2. Can we parametrise our model effectively using our dataset?

3. What generates the non-planarity of postures?

# The computational model

::: figure
![Schematic of Cosserat rod](./static/img/figure2.svg){ width=45% }
![Schematic of *C. elegans*](./static/img/celegans.svg){ width=45% }
<figcaption>Schematics of (left): Cosserat rod, (right): *C. elegans*</figcaption>
:::

::: { style="font-size:80%" }
Details of derivation in [@Wang2022] based on [@Cao2008].
:::

## The geometry of a rod

::: container
::: col
![Schematic of set up](./static/img/figure2.svg){ width=100% }
:::
::: col
- $\vec{x} \colon [0, 1] \to \R^3$ - a parametrisation of the midline curved
- $\{ \vec{d}_1, \vec{d}_2, \vec{d}_3 \} \colon [0, 1] \to SO^3$ - an orthonormal frame representing the orientation of the body cross sections about the midline
- The *Cosserat rod* formulation does not assume that $\vec{d}_3 \| \partial_s \vec{x}$ (but we expect it to be close).
:::
:::
[*Note that the dataset consists of $\vec{x}$ values only!*]{.mark}

## Parametrisation of the frame i

![Three rotations of the coordinate system: $\vec{R}_y, \vec{R}_x, \vec{R}_z$](./static/img/rotation.png){ width=50% }

$$
\begin{aligned}
\vec{Q} & = \vec{R}_z \vec{R}_x \vec{R}_y \\
& =
\small{
\left[
\begin{array}{ccc}
\cos\alpha \cos\gamma - \sin\alpha \sin\beta \sin\gamma & - \sin\alpha \cos\beta & \cos\alpha \sin\gamma + \sin\alpha \sin\beta \cos\gamma \\
\sin\alpha \cos\gamma + \cos\alpha \sin\beta \sin\gamma &  \cos\alpha \cos\beta & \sin\alpha \sin\gamma - \cos\alpha \sin\beta \cos\gamma \\
-\cos\beta \sin\gamma & \sin\beta & \cos\beta \cos\gamma\\
\end{array}
\right]
}
\end{aligned}
$$

## Parametrisation of the frame ii

$$
[\vec{d}_1(s, t), \vec{d}_2(s, t), \vec{d}_3(s, t)]
= [\vec{e}_1, \vec{e}_2, \vec{e}_3] \vec{Q}^T(s,t).
$$

Transfer between equations in the reference frame $(\cdot^{l})$ and global frame ($\cdot^g$):

$$
\vec{v}
= [\vec{e}_1, \vec{e}_2, \vec{e}_3]
\begin{pmatrix}
v_1^g \\ v_2^g \\ v_3^g
\end{pmatrix}
= [\vec{d}_1, \vec{d}_2, \vec{d}_3 ]
\vec{Q}
\begin{pmatrix}
v_1^g \\ v_2^g \\ v_3^g
\end{pmatrix}
=:
[\vec{d}_1, \vec{d}_2, \vec{d}_3 ]
\begin{pmatrix}
v_1^l \\ v_2^l \\ v_3^l
\end{pmatrix}
$$

## Conservation laws

Linear momentum balance (or [$\vec{x}$-equation]{.mark})
$$
\rho(s) A(s, t) \partial_{tt} \vec{x}(s, t)
= \partial_s \vec{n}(s, t) + \vec{f}(s, t)
$$

Angular momentum balance (or [$\vec{\theta}$-equation]{.mark})
$$
\partial_t \vec{h}(s, t)
= \partial_s \vec{m}(s, t)
+ \partial_s \vec{x}(s, t) \times \vec{n}(s, t) + \vec{l}(s, t)
$$

where

- $\vec{n}$ and $\vec{m}$ are the *internal* force and torque
- $\vec{f}$ and $\vec{l}$ are the *external* force and torque densities
- $\rho(s)$ and $A(s, t)$ are density and area of cross sections
- $\vec{h}$ is angular momentum
- formulated in [global coordinates]{.mark}

## Constitutive relations

We adopt a linear relationship between internal force $\vec{n}$ and strain $\vec{\epsilon}$ and between internal torque $\vec{m}$ and (generalised) curvature $\vec{\kappa}$:

$$
\begin{aligned}
\vec{n}^l & = \vec{K} \vec{\epsilon}^l,
&
\vec{\epsilon}^g(s, t) & := \partial_{\tilde{s}} \vec{r}(s, t) - \vec{d}_3 \\
\vec{m}^l & = \vec{J} \vec{\kappa}^l.
\end{aligned}
$$

::: { style="font-size:60%" }
$\tilde{s}$ is the reference configuration arc-length: $j = ds/d\tilde{s}$.
:::

$\vec{K}$ and $\vec{J}$ contain material parameters and terms for cross section shape:
$$
\begin{aligned}
\vec{K} &= \varphi^2 \diag(GA, GA, EA) &
\vec{J} &= \varphi^4 \diag(EI, EI, GI),
\end{aligned}
$$
where $E$ is Young's modulus, $G$ is shear modulus,\
$A$ cross sectional area, $I$ second moment of area, $\varphi$ tapered radius.



## Angular velocity and Curvature i

::: container
::: col
Recall frame vectors are orthonormal, we define **curvature** $\vec{\kappa}$ and **angular velocity** of the frame by:

$$
\begin{aligned}
\partial_t \vec{d}_i = \vec{\omega} \times \vec{d}_i \\
\partial_s \vec{d}_i = \vec{\kappa} \times \vec{d}_i.
\end{aligned}
$$
:::
::: col
![Rotation of a unit vector](./static/img/omega.PNG){width=50%}
:::
:::

## Angular velocity and Curvature ii

A small calculation allows to express $\vec{\kappa}$ and $\vec{\omega}$ in terms of the rotation angles $\vec{\theta} = (\alpha, \beta, \gamma)$:

$$
\vec{\omega}^l = \vec{A} \partial_t \vec{\theta}, \quad
\vec{\kappa}^l = \vec{A} \partial_s \vec{\theta}
$$
where
$$
\vec{A} = 
\begin{pmatrix}
0 & - \cos \alpha & \sin \alpha \cos \beta \\
0 & - \sin \alpha & - \cos \alpha \cos \beta \\
-1 & 0 & - \sin \beta
\end{pmatrix}.
$$

## The full model

Find $\vec{x}, \vec{\theta}$ such that
$$
\begin{aligned}
\rho \frac{A_0}{j} \partial_{tt} \vec{x}
& = \partial_s (j \vec{Q}^T \vec{K} \vec{Q} \partial_s \vec{x} - \vec{Q}^T \vec{K} \vec{e}_3) + \vec{f}
&& (\highlightsecond{\text{$\vec{x}$-equation}})
\end{aligned}
$$
$$
\begin{aligned}
& \partial_t ( \vec{Q}^T \vec{I} \vec{A} \partial_t \vec\theta )
= \partial_s (\vec{Q}^T \vec{J} \vec{A} \partial_s \vec{\theta} ) \\
& \qquad + \partial_s \vec{x} \times (j \vec{Q}^T \vec{K} \vec{Q} \partial_s \vec{x} - \vec{Q}^T \vec{K} \vec{e}_3) + \vec{l}
&& (\highlightsecond{\text{$\vec{\theta}$-equation}})
\end{aligned}
$$

Data: $\rho, A_0, \vec{I}, \vec{J}, \vec{K}, \vec{f}, \vec{l}$\
Geometric terms: $j, \vec{Q}, \vec{A}$ (depend on $\vec{x}$ and $\vec{\theta}$)

## The full model: *equation structure*

Find a $\highlight{\vec{x}}, \highlight{\vec{\theta}}$ such that
$$
\begin{aligned}
\highlightsecond{\rho} \frac{\highlightsecond{A_0}}{j} \highlight{\partial_{tt} \vec{x}}
& = \highlight{\partial_s} (j \vec{Q}^T \highlightsecond{\vec{K}} \vec{Q} \highlight{\partial_s \vec{x}} - \vec{Q}^T \highlightsecond{\vec{K}} \vec{e}_3) + \highlightsecond{\vec{f}} \\
\highlight{\partial_t} ( \vec{Q}^T \highlightsecond{\vec{I}} \vec{A} \highlight{\partial_t \vec\theta} )
& = \highlight{\partial_s} (\vec{Q}^T \highlightsecond{\vec{J}} \vec{A} \highlight{\partial_s \vec{\theta}} ) \\
& \qquad + \highlight{\partial_s \vec{x}} \times (j \vec{Q}^T \highlightsecond{\vec{K}} \vec{Q} \highlight{\partial_s \vec{x}} - \vec{Q}^T \vec{K} \vec{e}_3)
+ \highlightsecond{\vec{l}}.
\end{aligned}
$$

Data: $\highlightsecond{\rho}, \highlightsecond{A_0}, \highlightsecond{\vec{K}}, \highlightsecond{\vec{J}}, \highlightsecond{\vec{f}}, \highlightsecond{\vec{l}}$\
Geometric terms: $j, \vec{Q}, \vec{A}$ (depend on $\highlight{\vec{x}}$ and $\highlight{\vec{\theta}}$)

## Finite element formulation

::: container
::: col
::: { style="font-size:75%" }
$$
\begin{split}
& \int_{a_0}^{b_0}\rho A_0{\vec{v}} \cdot \partial_{tt}{\bf x} d\tilde{s} \\
& +\int_{a_0}^{b_0} \partial_{\tilde{s}}{\vec{v}} \cdot \left[{\bf Q}^T{\bf K}_0{\bf Q}\partial_{\tilde{s}}{\bf x}-K_{33}^0{\bf q}_3\right] d\tilde{s} \\
&+\int_{a_0}^{b_0}j{\vec{\psi}} \cdot \partial_t\left[j^{-2}{\bf Q}^T{\bf I}_0{\bf A}\partial_t{\boldsymbol{\theta}}\right] d\tilde{s} \\
& +\int_{a_0}^{b_0}j^{-3}\partial_{\tilde{s}} {\vec{\psi}} \cdot \left[{\bf Q}^T{\bf J}_0{\bf A}\partial_{\tilde{s}}{\boldsymbol{\theta}}\right] d\tilde{s}\\
&=\int_{a_0}^{b_0} j^{-1}{\vec{\psi}} \cdot {\bf B}\left[{\bf Q}^T{\bf K}_0{\bf Q}\partial_{\tilde{s}}{\bf x}-K_{33}{\bf q}_3\right] d\tilde{s} \\
& \qquad +\int_{a_0}^{b_0} j{\vec{\psi}} \cdot {\bf l} d\tilde{s}
+\int_{a_0}^{b_0} j{\vec{v}} \cdot {\bf f} d\tilde{s}.
\end{split}
$$
:::
:::
::: col
**Remarks**

- In reference configuration $\tilde{s}$ (total Lagrangian formulation)

- Test functions $\vec{v}$ (for $\vec{x}$) and $\vec{\psi}$ (for $\vec{\theta})$. All piecewise linear functions.

- Combined with backward Euler in time and fixed point iteration for nonlinear solve (sometimes capped at one iteration).
:::
:::

## Some results

::: container
::: col
![Bending a cantilever beam](https://yongxingwang.github.io/assets/video/beambend.gif)
:::
::: col
![Twisting and forcing a cantilever beam](https://yongxingwang.github.io/assets/video/beambendtwist.gif)
:::
:::

# Using the model to augment data

> We have a large dataset of *C. elegans* midlines over time. Can we work out what is the frame for each posture based on our model? What are the (internal and external) forces generate the motion?

[@Wang2022]

## The problem

> Given a sequence of midlines $(x_g, y_g, z_g)$, can we determine the frames $\vec\theta$ that *best* matches the data?

- We will work in the low Reynolds regime (appropriate for small swimmers) so neglect inertia
- Neglecting inertia in the fitting problem to a sequence of static problems

## Piecewise in time control

Given the state variables $\vec{x}_{n-1}$ and $\vec{\theta}_{n-1}$ at the previous time $t_{n-1}$, $n=1, 2, \ldots$, and an objective position vector $\vec{x}_g(t_n)$ of the worm's center line at current time $t_n$:

::: { style="font-size:75%" }
$$
\begin{split}
& \underset{{\bf f}_n,{\bf l}_n\in L^2\left([a,b]\right)}{\text{minimise}} \quad 
J(\vec{f}_n, \vec{l}_n, \vec{x}_n, \vec{\theta}_n)
=\frac{\lambda_g}{2}\int_{a}^{b}\left|{\bf x}_n-{\bf x}_g(t_n)\right|^2
+\frac{\lambda_f}{2}\int_{a}^{b}\left|{\bf f}_n\right|^2
+\frac{\lambda_l}{2}\int_{a}^{b}\left|{\bf l}_n\right|^2 \\
&+\frac{\lambda_d}{2}\int_{a}^{b}\left|\partial_s{\bf x}_n-{\bf d}_3({\boldsymbol{\theta}}_n)\right|^2
+\frac{1}{2}\int_{a}^{b}\left|{\boldsymbol{\Lambda}}_\kappa^{1/2}{\bf A}\partial_s{\boldsymbol{\theta}}_n\right|^2
+\frac{1}{2}\int_{a}^{b}\left|{\boldsymbol{\Lambda}}_\omega^{1/2}{\bf A}\frac{\left({\boldsymbol{\theta}}_n-{\boldsymbol{\theta}}_{n-1}\right)}{\Delta t}\right|^2,
\end{split}
$$
:::
subject to our model
$$
(\vec{x}_n, \vec{\theta}_n) = \mathcal{G}(\vec{f}_n, \vec{l}_n).
$$

::: { style="text-align:right" }
<small>
term by term next...
</small>
:::

## Objective term by term i

::: { style="font-size:75%" }
$$
\begin{split}
& \underset{{\bf f}_n,{\bf l}_n\in L^2\left([a,b]\right)}{\text{minimise}} \quad 
J(\vec{f}_n, \vec{l}_n, \vec{x}_n, \vec{\theta}_n)
=
\highlight{\frac{\lambda_g}{2}\int_{a}^{b}\left|{\bf x}_n-{\bf x}_g(t_n)\right|^2}
+\frac{\lambda_f}{2}\int_{a}^{b}\left|{\bf f}_n\right|^2
+\frac{\lambda_l}{2}\int_{a}^{b}\left|{\bf l}_n\right|^2 \\
&+\frac{\lambda_d}{2}\int_{a}^{b}\left|\partial_s{\bf x}_n-{\bf d}_3({\boldsymbol{\theta}}_n)\right|^2
+\frac{1}{2}\int_{a}^{b}\left|{\boldsymbol{\Lambda}}_\kappa^{1/2}{\bf A}\partial_s{\boldsymbol{\theta}}_n\right|^2
+\frac{1}{2}\int_{a}^{b}\left|{\boldsymbol{\Lambda}}_\omega^{1/2}{\bf A}\frac{\left({\boldsymbol{\theta}}_n-{\boldsymbol{\theta}}_{n-1}\right)}{\Delta t}\right|^2,
\end{split}
$$
:::

Data mismatch term for midline only

$\lambda_g$ large $\approx 1 / \int_a^b |\vec{x}_g|^2$

## Objective term by term ii

::: { style="font-size:75%" }
$$
\begin{split}
& \underset{{\bf f}_n,{\bf l}_n\in L^2\left([a,b]\right)}{\text{minimise}} \quad 
J(\vec{f}_n, \vec{l}_n, \vec{x}_n, \vec{\theta}_n)
=
\frac{\lambda_g}{2}\int_{a}^{b}\left|{\bf x}_n-{\bf x}_g(t_n)\right|^2
+
\highlight{\frac{\lambda_f}{2}\int_{a}^{b}\left|{\bf f}_n\right|^2}
+
\highlight{\frac{\lambda_l}{2}\int_{a}^{b}\left|{\bf l}_n\right|^2} \\
&+\frac{\lambda_d}{2}\int_{a}^{b}\left|\partial_s{\bf x}_n-{\bf d}_3({\boldsymbol{\theta}}_n)\right|^2
+\frac{1}{2}\int_{a}^{b}\left|{\boldsymbol{\Lambda}}_\kappa^{1/2}{\bf A}\partial_s{\boldsymbol{\theta}}_n\right|^2
+\frac{1}{2}\int_{a}^{b}\left|{\boldsymbol{\Lambda}}_\omega^{1/2}{\bf A}\frac{\left({\boldsymbol{\theta}}_n-{\boldsymbol{\theta}}_{n-1}\right)}{\Delta t}\right|^2,
\end{split}
$$
:::

Standard $L^2$-regularisation terms for $\vec{f}_n$ and $\vec{l}_n$ to avoid blow-up of controls.

## Objective term by term iii

::: { style="font-size:75%" }
$$
\begin{split}
& \underset{{\bf f}_n,{\bf l}_n\in L^2\left([a,b]\right)}{\text{minimise}} \quad 
J(\vec{f}_n, \vec{l}_n, \vec{x}_n, \vec{\theta}_n)
=
\frac{\lambda_g}{2}\int_{a}^{b}\left|{\bf x}_n-{\bf x}_g(t_n)\right|^2
+
\frac{\lambda_f}{2}\int_{a}^{b}\left|{\bf f}_n\right|^2
+
\frac{\lambda_l}{2}\int_{a}^{b}\left|{\bf l}_n\right|^2 \\
&+
\highlight{\frac{\lambda_d}{2}\int_{a}^{b}\left|\partial_s{\bf x}_n-{\bf d}_3({\boldsymbol{\theta}}_n)\right|^2
+\frac{1}{2}\int_{a}^{b}\left|{\boldsymbol{\Lambda}}_\kappa^{1/2}{\bf A}\partial_s{\boldsymbol{\theta}}_n\right|^2
+\frac{1}{2}\int_{a}^{b}\left|{\boldsymbol{\Lambda}}_\omega^{1/2}{\bf A}\frac{\left({\boldsymbol{\theta}}_n-{\boldsymbol{\theta}}_{n-1}\right)}{\Delta t}\right|^2},
\end{split}
$$
:::

Additional regularisation terms for $\vec{\theta}_n$

- Necessary for existence of the solution, physical deformations and to model anatomical constraints (asymmetries) on muscle forces.

- Replacing $\Lambda_\omega$ term with a Dirichlet boundary condition for $\vec\theta(a)$ is used in the first time step for a  well posed problem.

## Augmented Lagrangian

We introduce adjoint variables (Lagrange multipliers) $\hat{\vec{x}}$ and $\hat{\vec{\theta}}$ to eliminate the model constraint:

::: { style="font-size:75%" }
$$
\begin{aligned}
& L\left({\bf x}, {\boldsymbol{\theta}}, {\bf f}, {\bf l}, \hat{\bf x}, \hat{\boldsymbol{\theta}}\right) \\
& \, =
J(\vec{f}, \vec{l}, \vec{x}, \vec{\theta})
&& \highlightsecond{\text{(objective)}} \\
& \quad +\int_{a}^{b}\partial_s\hat{\bf x}^T\left({\bf Q}^T{\bf K}_0{\bf Q}\partial_s{\bf x}-j^{-1}K_{33}{\bf q}_3\right)
-\int_{a}^{b}\hat{\bf x}^T{\bf f}
&& \highlightsecond{\text{($\vec{x}$-equation)}} \\
& \quad +\int_{a}^{b}\partial_s\hat{\boldsymbol{\theta}}^T\left({\bf Q}^T{\bf J}{\bf A}\right)\partial_s{\boldsymbol{\theta}} \\
& \quad\qquad
-\int_{a}^{b}\hat{\boldsymbol{\theta}}^T{\bf B}\left({\bf Q}^T{\bf K}_0{\bf Q}\partial_s{\bf x}-j^{-1}K_{33}{\bf q}_3\right) -\int_{a}^{b}\hat{\boldsymbol{\theta}}^T{\bf l}
&& \highlightsecond{\text{($\vec\theta$-equation)}} \\
& \quad + \hat{\bf x}(b)^T\left[{\bf x}(b)-{\bf x}_g(b)\right]
- \hat{\bf x}(a)^T\left[{\bf x}(a)-{\bf x}_g(a)\right]
&& \highlightsecond{\text{($\vec{x}$-bc's)}} \\
& \quad
- \hat{\bf x}(b)^T{\bf n}(b)
+ \hat{\bf x}(a)^T{\bf n}(a)
&& \highlightsecond{\text{($\vec{\theta}$-bc's)}}
\end{aligned}
$$
:::

::: { style="font-size:75%" }
We've dropped the time step subscript for compactness.
:::

## Monolithic optimal control formulation i

- We derive Karush-Kuhn-Tucker (KKT) conditions to minimise $L$
- This results in primal equation, adjoint equation and optimality equations (details in the paper...)
- We combine all equations into a single monolithic formulation
- The resulting equation is highly nonlinear and coupled between all variables
- The problem is linearised using a fixed point iteration


## Monolithic optimal control formulation ii

:::{ style="font-size:55%" }

Given the state variables $\left({\bf x}_{n-1}, {\boldsymbol{\theta}}_{n-1}\right)$ at previous time $t_{n-1}$ ($n=1, 2, \ldots$), and an objective position vector ${\bf x}_g(t_n)$ at current time $t_n$, compute  $\left({\bf x}^k, {\boldsymbol{\theta}}^k\right) \rightarrow \left({\bf x}, {\boldsymbol{\theta}}\right) \in H_D^1$, $\left(\hat{\bf x}, \hat{\boldsymbol{\theta}} \right) \in H_0^1$ iteratively from $\left({\bf x}^0, {\boldsymbol{\theta}}^0\right)=\left({\bf x}_{n-1}, {\boldsymbol{\theta}}_{n-1}\right)$, such that $\forall \left(\vec{v}, \vec{\psi}\right)\in H_0^1$ and $\forall \left(\hat{\vec{v}}, \hat{\vec{\psi}}\right) \in H_0^1$:

$$
\begin{aligned}
&\int_{a}^{b}\partial_s{\hat{\vec{v}}}^T\left({\bf Q}_k^T{\bf K}_0{\bf Q}_k\right)\partial_s{\bf x}
+\partial_s{\hat{\vec{\psi}}}^T\left({\bf Q}_k^T{\bf J}{\bf A}_k\right)\partial_s{\boldsymbol{\theta}}
+\int_{a}^{b}\partial_s{\hat{\bf x}}^T\left({\bf Q}_k^T{\bf K}_0{\bf Q}_k\right)\partial_s{\vec{v}}
+\partial_s{\hat{\boldsymbol{\theta}}}^T\left({\bf Q}_k^T{\bf J}{\bf A}_k\right)\partial_s{\delta\boldsymbol{\theta}} \\
& \quad -\int_{a}^{b} {\hat{\boldsymbol{\theta}}}^T\left({\bf B}_k {\bf Q}_k^T{\bf K}_0{\bf Q}_k\right)\partial_s{\vec{v}}
+ {\hat{\vec{\psi}}}^T\left({\bf B}_k {\bf Q}_k^T{\bf K}_0{\bf Q}_k\right)\partial_s{\bf x}
+\lambda_g \int_{a}^{b}{\vec{v}}^T{\bf x}
-\frac{1}{\lambda_f}\int_{a}^{b}{\hat{\vec{v}}}^T{\hat{\bf x}}
-\frac{1}{\lambda_l}\int_{a}^{b}{\hat{\vec{\psi}}}^T{\hat{\boldsymbol{\theta}}} \\
& \quad + \int_{a}^{b}\partial_s{\vec{\psi}}^T{\bf A}_k^T{\boldsymbol{\Lambda}}_\kappa{\bf A}_k \partial_s{\boldsymbol{\theta}} 
+\frac{1}{\Delta t^2} \int_{a}^{b}{\vec{\psi}}^T{\bf A}_k^T{\boldsymbol{\Lambda}}_\omega{\bf A}_k{\boldsymbol{\theta}}
+\lambda_d\int_a^b \partial_s{\vec{v}}^T\partial_s{\bf x}
+\lambda_d\int_a^b \left(\delta\beta{\bf d}_\beta^k+
\delta\gamma{\bf d}_\gamma^k\right)^T\partial_s{\bf x}
- \partial_s{\vec{v}}^T\left(\beta{\bf d}_\beta^k+
\gamma{\bf d}_\gamma^k\right) \\
&\quad - \lambda_d\int_a^b  \left(\delta\beta{\bf d}_\beta^k+
\delta\gamma{\bf d}_\gamma^k\right)^T \left(\beta{\bf d}_\beta^k+
\gamma{\bf d}_\gamma^k\right)\\
& = \lambda_g \int_{a}^{b}{\vec{v}}^T{\bf x}_g
+\int_{a}^{b}j^{-1}K_{33}\partial_s{\hat{\vec{v}}}^T{\bf q}_3 
-\int_{a}^{b} j^{-1}K_{33}{\hat{\vec{\psi}}}^T{\bf B}_k {\bf q}_3^k
+\lambda_d\int_a^b \partial_s{\vec{v}}^T\left({\bf d}_3^k
-\beta^k{\bf d}_\beta^k-\gamma^k{\bf d}_\gamma^k\right)
+\lambda_d\int_a^b \left(\delta\beta{\bf d}_\beta^k+
\delta\gamma{\bf d}_\gamma^k\right){\bf d}_3^k\\
& \quad -\lambda_d\int_a^b  \left(\delta\beta{\bf d}_\beta^k+
\delta\gamma{\bf d}_\gamma^k\right)^T \left(\beta^k{\bf d}_\beta^k+
\gamma^k{\bf d}_\gamma^k\right)
+\frac{1}{\Delta t^2} \int_{a}^{b}{\vec{\psi}}^T{\bf A}_k^T{\boldsymbol{\Lambda}}_\omega{\bf A}_k{\boldsymbol{\theta}}_{n-1}.
\end{aligned}
$$

with

$$
\vec{d}_\beta^k = [\sin \beta^k, \sin \gamma^k, \cos \beta^k, -\sin \beta^k, \cos \gamma^k ]^T, \qquad
\vec{d}_\gamma^k = [-\cos \beta^k \cos \gamma^k, 0, -\cos \beta^k \sin \gamma^k]^T.
$$

:::

## Synthetic data testing

::: container
::: col
For verification, we use material parameters from *C. elegans* datasets but simulate data from a simplified beam set up
:::
::: col
![](./static/img/beam_diagram.PNG){ width=100% }
:::
:::

More details of simulation parameters in [@Wang2022].

## Test 1: converged solution

$f_2^l = -F_{\text{max}}(e^{4z/L_0}-1)/3, f_1^l = f_3^l = 0$

![](./static/img/test1_converged.png){ width=49% }
![](./static/img/test1_curvature.png){ width=49% }

## Test 1: convergence rates

![](./static/img/test1_convergence.png){ width=49% }
![](./static/img/test1_lambdad.png){ width=49% }

## Test 2: converged solution

$f_2^l = - 2.2 F_{\text{max}}, l_1^l = L_{\text{max}}$

![](./static/img/test2_converged.png){ width=50% }

## Test 2: convergence rates

![](./static/img/test2_convergence.png){ width=49% }
![](./static/img/test2_lambdarf.png){ width=49% }

## Results

![Reconstruction of *C. elegans*' locomotion by optimal control](https://yongxingwang.github.io/assets/video/worm.gif){ width=100% }

## $\vec{d}_3$-tangent detachment

![A small $\lambda_d$ means that $\vec{d}_3$ points in a different direction to the midline tangent $\partial_s \vec{x}$.](./static/img/d3.png){ width=80% }

## Head twisting

![The head undergoes a larger twist ($\vec{\kappa}_3^l$), much larger than the rest of the body.](./static/img/k3_700.png){ width=80% }

## Typical curvatures ($\Lambda_k = 0$)

![](./static/img/worm-basic-x.PNG){ width=80% }
![](./static/img/worm-basic-y.PNG){ width=80% }
![](./static/img/worm-basic-z.PNG){ width=80% }

## Typical curvatures ($(\Lambda_k)_3 \neq 0$)

![](./static/img/worm-test2-x.PNG){ width=80% }
![](./static/img/worm-test2-y.PNG){ width=80% }
![](./static/img/worm-test2-z.PNG){ width=80% }

<!---
# Some optimality results

> Why do organisms move the way they do?
> Why do organisms modulate (change) their undulation parameters in response to changing environments?

Some answers in 2d [@Deutz2024]

## A little bit of fluid dynamics

::: container
::: col
![](./static/img/rft.png)
:::
::: col
We assume forces and torques proportional to midline velocity $(\vec{u} = \partial_t \vec{x})$ and angular velocity $(\vec{\omega})$:
$$
\begin{aligned}
\vec{f}_{\text{D}} &= - \mu \left[\,c_{\parallel}\vec{t}\otimes\vec{t}+c_{\perp}\,\left(\vec{1}-\vec{t}\otimes\vec{t}\right)\,\right]\vec{u},\\
\vec{l}_{\text{D}} &= -\mu \left[\gamma_{\parallel}\vec{t}\otimes\vec{t}+l\gamma_{\perp}\left(\vec{1}-\vec{t}\otimes\vec{t}\right)\right]\vec{\omega}.
\end{aligned}
$$
:::
:::


## Model reformulation

To model undulation across different environmental conditions, we must now also include internal viscous effects:

$$
\begin{aligned}
\vec{m}^l & = \vec{K} \vec\epsilon^l + \tilde{\vec{K}} \partial_t \vec\epsilon^l \\
\vec{n}^l & = \vec{J} \vec{\kappa}^l + \tilde{\vec{J}} \partial_t \vec{\kappa}^l.
\end{aligned}
$$


## Some important material parameters

$$
\begin{aligned}
\vec{K} &= \varphi^2 \diag(GA, GA, EA) &
\tilde{\vec{K}} & = \varphi^2 \diag(\nu A, \nu A, \eta A) \\
\vec{J} &= \varphi^4 \diag(EI, EI, GI) &
\tilde{\vec{J}} & = \varphi^4 \diag(\eta I, \eta I , \nu I).
\end{aligned}
$$

where

::: container
::: col
- Young's modulus $E > 0$
- Shear modulus $G > 0$
:::
::: col
- Extensional viscosity $\eta \ge 0$
- Shear viscosity $\nu \ge 0$
:::
:::
- $I$, $A$ and $\varphi$ related to geometry

## Some important non-dimensional parameters

Important time scales
$$
\tau = \frac{\mu c_{\parallel} L_0^4}{EI}, \qquad
\xi = \frac{\eta}{E}
$$

::: { style="font-size:60%" }
$\tau$ is proportional to fourth power of Sperm number.
:::

Non-dimensionalised by considering a characteristic timescale $T$ (typically time period of undulations):

$$
a = \frac{\tau}{T}, \qquad
b = \frac{\xi}{T}.
$$


## Periodic travelling wave-muscles

::: container
::: col
Actuation torque (from muscles) is defined through a preferred curvature:
$$
\vec{L}_A = - E I \vec{\kappa}_0, \qquad
\vec{\kappa}_0(s, t) = A_0 \sin(q_0 s - \omega_0 t) \vec{d}_1.
$$

We can measure output wave parameters similarly:
$$
\vec{\kappa} = \vec{A} \partial_s \vec{\theta}, \qquad
\kappa(s, t) \approx A \sin(q s - \omega t - \phi) \vec{d}_1.
$$
:::

::: col
![Sample wave profiles for different values of $c_0 = A_0 / q_0$](./static/img/undulation.png)
:::
:::


## Simulation results

![There are three district swimming regimes: efficient swimming, transition and struggle.\
<small>$\phi$ is phaselag between preferred and actual curvature, $A$ is curvature amplitude and $U$ is swimming speed.</small>](./static/img/regime_1.svg){ .r-stretch }

## Energy expenditure (definitions)

::: { style="font-size:90%" }
$$
\begin{aligned}
V(t)  &=\frac{1}{2}\int_{0}^{L_{0}}\boldsymbol{\kappa}^l\cdot\boldsymbol{J}\boldsymbol{\kappa}^l+\boldsymbol{\epsilon}^l\cdot\boldsymbol{K}\boldsymbol{\epsilon}^l\,ds
&& \highlightsecond{(\text{potential energy})} \\ 
\label{eq: D_I_dot}
\dot{D}_{\text{I}}(t)&=-\int_{0}^{L_{0}}\partial_{t}\boldsymbol{\kappa}^l\cdot\tilde{\boldsymbol{J}}\partial_{t}\boldsymbol{\kappa}^l+\partial_{t}\boldsymbol{\epsilon}^l\cdot\tilde{\boldsymbol{K}}\partial_{t}\boldsymbol{\epsilon}^l\,ds
&& \highlightsecond{(\text{internal dissipation})} \\ 
\label{eq: D_F_dot}
\dot{D}_{\text{F}}(t)&=\int_{0}^{L_{0}}\boldsymbol{f}_{F}\cdot\boldsymbol{u}+\boldsymbol{l}_{F}\cdot\boldsymbol{\omega}\,ds,
&& \highlightsecond{(\text{external dissipation})} \\ 
\label{eq: W_dot}
\dot{W}(t)  &=\int_{0}^{L_{0}}\boldsymbol{f}_{\text{A}}\cdot\boldsymbol{u}+\boldsymbol{l}_{\text{A}}\cdot\boldsymbol{\omega}\,ds
&& \highlightsecond{(\text{work done})}
\end{aligned}
$$

Note that over a single undulation
$$
0 = W + D_I + D_F
$$
So that work done ($W$) matches the sum of dissipation ($D_I + D_F$)
:::

## Energy expenditure (results)

![The most efficient strategy is not to go very slowly!\
[$W$ is total energy spent, $D_I$ is internal dissipation, $D$ is total dissipation, $\COT$ is cost of transport $W/S$]{ style="font-size:65%"}.](./static/img/energies.svg){ .r-stretch }

## The cost of being alive (definitions)

We define the *cost of transport* (COT) as required action per unit distance
$$
\COT = \frac{W}{S}
$$

We find optimising $\COT$ on its own results in unphysically low speeds.

[Ideal]{.mark}: Introduce *basal metabolic rate* (BMR) to represent the cost of being alive
$$
\COT^* = \frac{W + \BMR \cdot T}{S}.
$$

## The cost of being alive (results)

![Depending on the basal metabolic rate (BMR) a different optimum appears.\
[Left: $\BMR = 0.1$, Right, $\BMR = 10$]{ style="font-size:65%"}](./static/img/cot_bmr.svg){ .r-stretch }

## Comparison with *C. elegans* data i

fig 5.1E

different viscocities
fig 5.2ADE
fig 5.5(some)

![Measurements of *C. elegans* undulation frequency matches an optimal value in water](./static/img/celegans_frequency_modulation_water.svg){ .r-stretch }

## Comparison with *C. elegans* data ii

![*C. elegans* waveform matches most energy efficient parameters\
[Black line is value that gives maximum speed, black squares are measured data, red region is minimal $\COT$.]{ style="font-size:65%" }](./static/img/celegans_optimal_waveform.svg)

--->

# Thank you for your attention! { data-background="https://yongxingwang.github.io/assets/video/worm.gif"}

## References
